% Copyright © 2023-2024
% Dr. Roman Vetter
% Department of Biosystems Science and Engineering
% ETH Zurich

function [lambda0, alpha] = spectralanalysis(varargin)

assert(2 <= nargin && nargin <= 4)

h = varargin{1}; % 2D height map
L = varargin{2}; % length of longer image edge in µm

createplots = false; % don't create plots by default
if nargin > 2
    createplots = varargin{3};
end

filename = ''; % don't write results to file by default
if nargin > 3
    filename = varargin{4};
end

h = rescale(h);

% convert to grayscale if necessary
if ndims(h) == 3
    h = mean(h,3);
end

[m, n] = size(h);
res = L / max(m,n); % pixel resolution in µm
Ly = res * m;
Lx = res * n;

if createplots
    figure('Position', [0 0 1200 500])
    subplot(1,2,1)
    imshow(h, [], 'XData', [0, Lx], 'YData', [0, Ly])
    axis('on', 'image')
end

% Fourier transform
F = fft2(h) * res^2;
A = fftshift(abs(F)); % spectral amplitude
S = fftshift(abs(F).^2); % power spectral density

% store an image of the Fourier magnitude
% logM = log10(A);
% lo = ceil(min(logM,[],'all'))
% hi = floor(max(logM,[],'all'))
% imwrite(uint8((logM - lo) / (hi - lo) * 256), plasma, "magnitude.png");

% remove the DC component
v0 = 1 + floor(m/2);
u0 = 1 + floor(n/2);
A(v0, u0) = 0;

Nk = 100000; % number of wavenumbers
Nphi = 360; % number of angles for averaging

% grid for evaluation
kmaxy = 2 * pi / Ly * floor((m-1)/2);
kmaxx = 2 * pi / Lx * floor((n-1)/2);
kmax = min(kmaxx, kmaxy);
k = (1:Nk)/Nk * kmax;
k = logspace(-4, log10(kmax), Nk);
phi = (1:Nphi)/Nphi * pi;
[K, PHI] = meshgrid(k, phi);
v = v0 + K .* sin(PHI) * Ly / (2 * pi);
u = u0 + K .* cos(PHI) * Lx / (2 * pi);

% average over all angles to extract the isotropic part
A = mean(interp2(A, u, v), 1);
S = mean(interp2(S, u, v), 1);

% approximate the dominant mode
G = @(p,x) p(3) * exp(-(x-p(1)).^2/(2*p(2)^2)) + p(4);
[~,i0] = max(A .* k); % initial guess
mdl = fitnlm(log(k), A .* k, G, [log(k(i0)),1,100,10]);
p = mdl.Coefficients.Estimate;
k0 = exp(p(1));
lambda0 = 2 * pi / k0; % dominant wavelength in µm

if createplots
    line(Lx/20+[0 lambda0], Ly*19/20*[1 1], 'Color', 'yellow', 'LineWidth', 4)
    text(Lx/20+lambda0/2, Ly*18/20, ['\lambda_0=' num2str(round(lambda0)) 'µm'], 'Color', 'yellow', 'FontSize', 18, 'HorizontalAlignment', 'center')
    xlabel('x (µm)')
    ylabel('y (µm)')
    set(gca, 'LineWidth', 1, 'FontSize', 18)

    subplot(1,2,2)
    box on
    hold on
    grid on
    plot(k, A, '.', 'DisplayName', 'A: Spectral amplitude')
    plot(k, S, '.', 'DisplayName', 'S: Power spectral density')
    plot(k, G(p,log(k)) ./ k, 'k-')
    set(gca, 'XScale', 'log', 'YScale', 'log')
    legend show
    xlabel('Wavenumber k (1/µm)')
    set(gca, 'LineWidth', 1, 'FontSize', 18)
    
    plot(k0, interp1(k, A, k0), 'ko', 'LineWidth', 1, 'HandleVisibility', 'off')
end

idx = k > 1.5 * k0 & k < max(k) / 2.5;
mdlA = fitnlm(log(k(idx)), log(A(idx)), @polyval, [0 0]);
mdlS = fitnlm(log(k(idx)), log(S(idx)), @polyval, [0 0]);
alpha = -mdlA.Coefficients.Estimate(1);

if createplots
    plot(k(idx), exp(polyval(mdlA.Coefficients.Estimate, log(k(idx)))), 'k-', 'LineWidth', 1, 'HandleVisibility', 'off')
    plot(k(idx), exp(polyval(mdlS.Coefficients.Estimate, log(k(idx)))), 'k-', 'LineWidth', 1, 'HandleVisibility', 'off')
    text(0.75, 0.75, ['\alpha = ' sprintf('%.2f', alpha)], 'Units', 'normalized', 'FontSize', 18)
end

% write results to file
if ~isempty(filename)
    T = table();
    T.k = [k(1:100:end)'; k(end)];
    T.A = [A(1:100:end)'; A(end)];
    T.S = [S(1:100:end)'; S(end)];
    writetable(T, filename);
end

end