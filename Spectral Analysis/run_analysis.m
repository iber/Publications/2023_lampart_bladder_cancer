% Copyright © 2023-2024
% Dr. Roman Vetter
% Department of Biosystems Science and Engineering
% ETH Zurich

clear all
close all
figure('Position', [0 0 1500 400])
fontSize = 18;
dotSize = 250;
lambda_label = 'Dominant wavelength \lambda_0 (µm)';
alpha_label = 'Spectral exponent \alpha';

createplots = false;
recompute = true;

lambda_BBN = [];
alpha_BBN = [];
[lambda_BBN(end+1), alpha_BBN(end+1)] = spectralanalysis(imread('images/BBN_MBC8B1_S3_A1_4_3.67um.png'), 3.67*200, createplots);
[lambda_BBN(end+1), alpha_BBN(end+1)] = spectralanalysis(imread('images/BBN_MBC8B1_S4B_A1_2_3.67um.png'), 3.67*230, createplots);
[lambda_BBN(end+1), alpha_BBN(end+1)] = spectralanalysis(imread('images/BBN_MBC9_S5_4.77um.png'), 4.77*330, createplots, 'kAS.csv');

T = table();
T.wavelength_um = lambda_BBN';
T.exponent = alpha_BBN';
writetable(T, 'wavelength_exponent_BBN.csv')

lambda_CTRL = [];
alpha_CTRL = [];
[lambda_CTRL(end+1), alpha_CTRL(end+1)] = spectralanalysis(imread('images/CTRL_MBC8B1_S1B_A2_2_4.53um.png'), 4.53*210, createplots);
[lambda_CTRL(end+1), alpha_CTRL(end+1)] = spectralanalysis(imread('images/CTRL_MBC8B1_S1T_A1_2_4.53um.png'), 4.53*210, createplots);
[lambda_CTRL(end+1), alpha_CTRL(end+1)] = spectralanalysis(imread('images/CTRL_MBC8B1_S2B_A1_2_4.53um.png'), 4.53*393, createplots);
[lambda_CTRL(end+1), alpha_CTRL(end+1)] = spectralanalysis(imread('images/CTRL_MBC8B1_S2B_A2_5_4.53um.png'), 4.53*215, createplots);
[lambda_CTRL(end+1), alpha_CTRL(end+1)] = spectralanalysis(imread('images/CTRL_MBC9_S2_4.77um.png'), 4.77*280, createplots);
[lambda_CTRL(end+1), alpha_CTRL(end+1)] = spectralanalysis(imread('images/CTRL_MBC9_S3_4.77um.png'), 4.77*200, createplots);
[lambda_CTRL(end+1), alpha_CTRL(end+1)] = spectralanalysis(imread('images/CTRL_MBC9_S4_4.77um.png'), 4.77*200, createplots);

T = table();
T.wavelength_um = lambda_CTRL';
T.exponent = alpha_CTRL';
writetable(T, 'wavelength_exponent_CTRL.csv')

N = 169;

x = repmat(logspace(-4,-1,13)', 13, 1);
y = kron(logspace(-1,-4,13)', ones(13,1));
[X, Y] = meshgrid(logspace(-4,-1,13), logspace(-1,-4,13));

lambda = NaN(N,1);
alpha = NaN(N,1);
if recompute
    parfor i = 1:N
        if log10(y(i)) > log10(x(i)) - 2.3
            h = imread(['simulations_left/' num2str(i) '_32bit.png']);
            [lambda(i), alpha(i)] = spectralanalysis(h, 600);
        end
    end
    T1 = table();
    T1.EEP_EBM = x;
    T1.ELP_EBM = y;
    T1.wavelength_um = lambda;
    T1.exponent = alpha;
    writetable(T1, 'simulations1.csv');
    T1.wavelength_um(isnan(T1.wavelength_um)) = max(T1.wavelength_um);
    writetable(T1, 'simulations1ext.csv');
else
    T1 = readtable('simulations1.csv');
    lambda = T1.wavelength_um;
    alpha = T1.exponent;
end

subplot(1,3,1)
hold on
scatter(x, y, dotSize, lambda, "filled")
levels = [40 80 120 160];
C = contour(X, Y, reshape(lambda,13,13)', levels, 'LineWidth', 2, 'EdgeColor', 'none');
m = 1;
for i = 1:length(levels)
    m(end+1) = m(end) + C(2,m(end)) + 1;
end
xi = -4:0.05:-1;
for i = 1:length(levels)
    xc = log10(C(1,m(i)+1:m(i+1)-1));
    yc = log10(C(2,m(i)+1:m(i+1)-1));
    mdl = fitnlm(xc, yc, @polyval, zeros(1,3))
    plot(10.^xi, 10.^(polyval(mdl.Coefficients.Estimate,xi)), 'k-', 'LineWidth', 1)
end
set(gca, 'XScale', 'log', 'YScale', 'log', 'FontSize', fontSize)
xlabel('Stiffness ratio E_{EP}/E_{BM}')
ylabel('Stiffness ratio E_{LP}/E_{BM}')
xlim([1e-4 1e-1])
ylim([1e-4 1e-1])
cb = colorbar;
ylabel(cb, lambda_label, 'FontSize', fontSize)
clim([0 250])
box on

subplot(1,3,3)
hold on
scatter(lambda, alpha, 'k.')
scatter(lambda_BBN, alpha_BBN, 'ro', 'LineWidth', 2)
scatter(lambda_CTRL, alpha_CTRL, 'go', 'LineWidth', 2)
box on
xlabel(lambda_label)
ylabel(alpha_label)
set(gca, 'XScale', 'log', 'FontSize', fontSize)
xlim([1e1 1e3])
ylim([1.4 2.2])

x = repmat(logspace(2,5,13)', 13, 1);
y = kron(logspace(3,0,13)', ones(13,1));
[X, Y] = meshgrid(logspace(2,5,13), logspace(3,0,13));

lambda = NaN(N,1);
alpha = NaN(N,1);
if recompute
    parfor i = 1:N
        if log10(y(i)) > log10(x(i)) - 4.6 && log10(y(i)) < log10(x(i)) + 0.01
            h = imread(['simulations_right/' num2str(i) '_32bit.png']);
            [lambda(i), alpha(i)] = spectralanalysis(h, 600);
        end
    end
    T2 = table();
    T2.EBM_EEP = x;
    T2.ELP_EEP = y;
    T2.wavelength_um = lambda;
    T2.exponent = alpha;
    writetable(T2, 'simulations2.csv');
    R = rot90(triu(true(13),1));
    T2.wavelength_um(isnan(T2.wavelength_um) & R(:)) = min(T2.wavelength_um);
    T2.wavelength_um(isnan(T2.wavelength_um)) = max(T2.wavelength_um);
    writetable(T2, 'simulations2ext.csv');
else
    T2 = readtable('simulations2.csv');
    lambda = T2.wavelength_um;
    alpha = T2.exponent;
end

subplot(1,3,2)
hold on
scatter(x, y, dotSize, lambda, "filled")
levels = [20 40 80];
C = contour(X, Y, reshape(lambda,13,13)', levels, 'LineWidth', 2);
m = 1;
for i = 1:length(levels)
    m(end+1) = m(end) + C(2,m(end)) + 1;
end
xi = 2:0.05:5;
for i = 1:length(levels)
    xc = log10(C(1,m(i)+1:m(i+1)-1));
    yc = log10(C(2,m(i)+1:m(i+1)-1));
    mdl = fitnlm(xc, yc, @polyval, zeros(1,3))
    plot(10.^xi, 10.^(polyval(mdl.Coefficients.Estimate,xi)), 'k-', 'LineWidth', 1)
end
set(gca, 'XScale', 'log', 'YScale', 'log', 'FontSize', fontSize)
xlabel('Stiffness ratio E_{BM}/E_{EP}')
ylabel('Stiffness ratio E_{LP}/E_{EP}')
xlim([1e2 1e5])
ylim([1e0 1e3])
cb = colorbar;
ylabel(cb, alpha_label, 'FontSize', fontSize)
clim([0 250])
box on

subplot(1,3,3)
scatter(lambda, alpha, 'k.')
