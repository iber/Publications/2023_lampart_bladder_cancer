{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9f356a1e",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Copyright © 2023\n",
    "#ETH Zurich\n",
    "#Department of Biosystems Science and Engineering\n",
    "#Franziska Lampart, Roman Vetter, Kevin A. Yamauchi, Yifan Wang, Steve Runser, Nico Strohmeyer, Florian Meer, Marie-Didiée Hussherr, \n",
    "#Gieri Camenisch, Hans-Helge Seifert, Cyrill A. Rentsch, Clémentine Le Magnen, Daniel J. Müller, Lukas Bubendorf, Dagmar Iber"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d35cbbec",
   "metadata": {},
   "source": [
    "# Overview\n",
    "\n",
    "This notebook computes the distances between the surfaces of the epithelium and basement membrane.\n",
    "\n",
    "**Notes**:\n",
    "- these measurements assume the voxels are isotropic (i.e., same physical size along each dimension).\n",
    "- Before starting the analysis make sure the faces of the EP_mesh have the correct orientation. The normal vectors should point towards the BM_mesh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1ba9539f",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Load libraries\n",
    "import napari\n",
    "import trimesh\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "from matplotlib import pyplot as plt "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "741186aa",
   "metadata": {},
   "source": [
    "### Reading in the meshes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41cdcbf2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the meshes\n",
    "epi_mesh = trimesh.load('EP_mesh.obj') \n",
    "bm_mesh = trimesh.load('BM_mesh.obj')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7f8581b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate surfaces\n",
    "epi_verts = epi_mesh.vertices\n",
    "epi_faces = epi_mesh.faces\n",
    "epi_values = np.ones((len(epi_verts),))\n",
    "\n",
    "bm_verts = bm_mesh.vertices\n",
    "bm_faces = bm_mesh.faces\n",
    "bm_values = np.ones((len(bm_verts),))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c52a1d5",
   "metadata": {},
   "source": [
    "### Normal ray casting and distance measurement"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "573ad1e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get face normals\n",
    "epi_normals = epi_mesh.vertex_normals\n",
    "bm_normals = bm_mesh.vertex_normals"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b06669d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the intersection of the rays from the EP_mesh to the BM_mesh.\n",
    "# This step can take a few minutes!\n",
    "locations, index_ray, index_tri = bm_mesh.ray.intersects_location(\n",
    "    ray_origins=epi_verts,\n",
    "    ray_directions=epi_normals,\n",
    "    multiple_hits=False\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "029b9bdd",
   "metadata": {},
   "source": [
    "In the cell below, we compute the distance of the rays. To convert the distance into physical units, you will be prompted for the pixel size. Please enter the pixel size in microns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "64f9b848",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Filter rays without hits and calculate the length of the ray in um\n",
    "verts_with_hits = epi_verts[index_ray]\n",
    "disp_vector = verts_with_hits - locations\n",
    "distances = np.linalg.norm(disp_vector, axis=1)\n",
    "\n",
    "inputdis = float(input(\"Please enter pixel size: \"))\n",
    "distances_um = distances * inputdis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0e5a1172",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Inspect distance distribution\n",
    "plt.hist(distances_um, bins=300)\n",
    "plt.xlabel('Distance(um)')\n",
    "plt.ylabel('Frequency')\n",
    "plt.title('Distance between BM and Epithelium')\n",
    "axes = plt.gca()\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a342b15b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Export distance measurements as csv\n",
    "np.savetxt('Tissue_thickness.csv', distances_um, delimiter=',')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c277820",
   "metadata": {},
   "source": [
    "### Visualisation in Napari"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e1e38a35",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Visualise Surface\n",
    "viewer = napari.view_surface((epi_verts, epi_faces, epi_values), name = 'epi', colormap='green')\n",
    "viewer.add_surface((bm_verts, bm_faces, bm_values), name = 'bm')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f5df6ea3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display distance map\n",
    "distance_values = np.zeros((len(epi_verts), ) )\n",
    "distance_values[index_ray] = distances_um\n",
    "viewer.add_surface((epi_verts, epi_faces, distance_values), name = 'Thickness Normal_Ray', \n",
    "                   colormap='viridis', shading = 'none')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
