//Copyright © 2023
//ETH Zurich
//Department of Biosystems Science and Engineering
//Franziska L. Lampart, Roman Vetter, Kevin A. Yamauchi, Yifan Wang, Steve Runser, Nico Strohmeyer, Florian Meer, Marie-Didiée Hussherr,
//Gieri Camenisch, Hans-Helge Seifert, Cyrill A. Rentsch, Clémentine Le Magnen, Daniel J. Müller, Lukas Bubendorf, Dagmar Iber

run("Subtract Background...", "rolling=75 stack");
run("Gaussian Blur 3D...", "x=2 y=2 z=2");
run("Morphological Filters (3D)", "operation=[White Top Hat] element=Cube x-radius=5 y-radius=5 z-radius=5");
run("Morphological Filters (3D)", "operation=Dilation element=Square x-radius=2 y-radius=2 z-radius=2");
run("Gaussian Blur 3D...", "x=5 y=5 z=5");
run("Morphological Filters (3D)", "operation=Erosion element=Square x-radius=5 y-radius=5 z-radius=5");

