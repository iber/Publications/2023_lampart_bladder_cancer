#Copyright © 2023
#ETH Zurich
#Department of Biosystems Science and Engineering
#Franziska L. Lampart, Roman Vetter, Kevin A. Yamauchi, Yifan Wang, Steve Runser, Nico Strohmeyer, Florian Meer, Marie-Didiée Hussherr,
#Gieri Camenisch, Hans-Helge Seifert, Cyrill A. Rentsch, Clémentine Le Magnen, Daniel J. Müller, Lukas Bubendorf, Dagmar Iber

"""Create a viewer to curate a boundary segmentation. The curation is semi-automated
	and requires the user to choose which labels they want to fix.

Before running this script, please install morphometrics:

To use, set the following variables below. Paths can be relative.
	RAW_IMAGE_PATH: path to the raw images
	FILTERED_IMAGE_PATH: path to the filtered image
	PREDICTIONS_PATH: path to the boudnary prediction
	CURATED_LABELS_PATH: path to the file to save the
						 curated boundaries to.
						 Must be an h5 file.

The two most important paths for curation are:
	RAW_SEGMENTATION PATH: the segmented image that needs to be curated
	TISSUE_SEGMENTATION PATH: the binary mask


Hot keys:
	q: view just the raw image
	w: view just the filtered image
	e: toggle on the edges
	f: fix and save (as a .tif file) the labels in the array labels_to_fix
	Double left-click: append the label clicked to the array labels_to_fix
"""

import os
from re import X
from typing import List
import warnings

import h5py
from magicgui import magic_factory, widgets
import napari
from napari.layers import Labels
from napari.qt.threading import thread_worker, FunctionWorker
from napari.types import LayerDataTuple
import numpy as np
import pyclesperanto_prototype as cle
from skimage.io import imread
from skimage.io import imsave
from skimage.morphology import binary_dilation, cube
from skimage.segmentation import find_boundaries, expand_labels
import time
from datetime import datetime

from morphometrics._gui.label_curator.qt_label_curator import QtLabelingWidget

# set data sources
RAW_IMAGE_PATH = "filename_GFP_clean.tif"
FILTERED_IMAGE_PATH = "filename_GFP_filtered_clean.tif"
RAW_SEGMENTATION_PATH = "filename_segmentation.tif"
NUCLEI_IMAGE_PATH = "filename_DAPI_clean.tif"
TISSUE_SEGMENTATION_PATH = "filename_GFP_tissue_mask.tif"

# path to save the curated labels
CURATED_LABELS_PATH = "filename_curated_cells.h5"

# configure curation
EDGE_PAINTING_BRUSH_SIZE = 2
SMALL_ERASE_BRUSH_SIZE = 10
BIG_ERASE_BRUSH_SIZE = 40
EXPAND_AMOUNT = 3
TISSUE_PROBABILITIES_THRESHOLD = 0.5

# load the raw image
raw_im = imread(RAW_IMAGE_PATH)

# load the filtered image
if FILTERED_IMAGE_PATH == "":
	filtered_im = None
else:
	filtered_im = imread(FILTERED_IMAGE_PATH)

# load the nuclei image
nuclei_im = imread(NUCLEI_IMAGE_PATH)

# load the segmentation
if os.path.isfile(CURATED_LABELS_PATH):
	# load the curated segmentation
	with h5py.File(CURATED_LABELS_PATH, "r") as f:
		segmentation = f["predictions"][:]
		try:
			validated_labels = f["validated_labels"][:]
		except KeyError:
			validated_labels = np.zeros_like(segmentation)

else:
	# if the segmentation doesn't exist,
	# load the original segmentation
	segmentation = imread(RAW_SEGMENTATION_PATH)
	validated_labels = np.zeros_like(segmentation)


	# create the segmentation file
	with h5py.File(CURATED_LABELS_PATH, "w") as f:
		f.create_dataset(
			"predictions",
			data=segmentation,
			compression="gzip"
		)
		f.create_dataset(
			"validated_labels",
			data=validated_labels,
			compression="gzip"
		)

# create the boundary image
boundary_image = find_boundaries(segmentation)

# load the tissue segmentation
if TISSUE_SEGMENTATION_PATH != "":
	background_mask = imread(TISSUE_SEGMENTATION_PATH).astype(bool)
else:
	background_mask = np.zeros_like(segmentation, dtype=bool)

# make the viewer
viewer = napari.Viewer()

# add the images
raw_layer = viewer.add_image(raw_im)

if filtered_im is not None:
	filtered_layer = viewer.add_image(filtered_im)

background_layer = viewer.add_image(background_mask, visible=False)

boundary_layer = viewer.add_image(
	boundary_image,
	blending="additive",
	visible=False
)
nuclei_layer = viewer.add_image(
	nuclei_im,
	opacity=0.7,
	blending="additive",
	colormap="bop blue"
)

validated_labels_layer = viewer.add_labels(
	validated_labels,
	name="validated labels",
	visible=False
)

# add the segmentation
segmentation_layer = viewer.add_labels(
	segmentation,
	name="segmentation"
)
segmentation_layer.preserve_labels = True


def _view_selected_layer(layer_to_view):
	if not isinstance(layer_to_view, list):
		layer_to_view = [layer_to_view]
	for layer in viewer.layers:
		if layer in layer_to_view:
			layer.visible = True
		else:
			layer.visible = False


def _get_visible_state():
	"""Get the visible state for each layer in the viewer"""
	return {layer: layer.visible for layer in viewer.layers}


def _set_visible_state(visible_state):
	"""Set the visible state for each layer in the viewer"""
	for layer in viewer.layers:
		layer.visible = visible_state[layer]


def _select_segmentation_layer():
	# Set the segmentation layer as selected
	viewer.layers.selection = {segmentation_layer}


@viewer.bind_key("q")
def toggle_raw_focus(viewer):
	visible_state = _get_visible_state()
	_view_selected_layer([raw_layer, nuclei_layer])
	
	yield
	_set_visible_state(visible_state)


@viewer.bind_key("w")
def toggle_filtered_focus(viewer):
	visible_state = _get_visible_state()
	_view_selected_layer([filtered_layer, nuclei_layer])
	
	yield
	_set_visible_state(visible_state)


@viewer.bind_key("e")
def toggle_boundary(viewer):
	boundary_layer.visible = True
	
	yield
	boundary_layer.visible = False


@viewer.bind_key("r")
def expand_segmentation_labels(viewer):
	label_image = segmentation_layer.data
	expanded_labels = expand_labels(label_image, EXPAND_AMOUNT)

	# trim off the expansion that went outside of the tissue
	expanded_labels[background_mask] = 0

	# set the data
	segmentation_layer.data = expanded_labels


@viewer.bind_key("s")
def save_checkpoint(viewer):
	print(f"saving to {CURATED_LABELS_PATH}")
	current_segmentation = segmentation_layer.data.astype(int)
	validated_labels = validated_labels_layer.data.astype(int)
	with h5py.File(CURATED_LABELS_PATH, 'r+') as f:
		# save the segmentation
		curated_segmentation = f["predictions"]
		curated_segmentation[...] = current_segmentation

		# save the validated bales
		validated_labels_dataset = f["validated_labels"]
		validated_labels_dataset[...] = validated_labels

	warnings.warn(f"saved to: {CURATED_LABELS_PATH}")
	print("saved")



# add the expansion curation widget
widget = QtLabelingWidget(viewer)
viewer.window.add_dock_widget(widget)

_select_segmentation_layer()



if __name__ == '__main__':
	# start the viewer
	print("starting viewer")
	napari.run()
