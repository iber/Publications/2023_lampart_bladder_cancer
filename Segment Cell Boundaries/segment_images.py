#Copyright © 2023
#ETH Zurich
#Department of Biosystems Science and Engineering
#Franziska L. Lampart, Roman Vetter, Kevin A. Yamauchi, Yifan Wang, Steve Runser, Nico Strohmeyer, Florian Meer, Marie-Didiée Hussherr,
#Gieri Camenisch, Hans-Helge Seifert, Cyrill A. Rentsch, Clémentine Le Magnen, Daniel J. Müller, Lukas Bubendorf, Dagmar Iber

import numpy as np
from skimage.io import imread, imsave
from skimage.segmentation import relabel_sequential

from plantseg.dataprocessing.functional import image_rescale
from plantseg.predictions.functional import unet_predictions
from plantseg.predictions.functional.utils import STRIDE_ACCURATE
from plantseg.segmentation.functional import dt_watershed, lifted_multicut_from_nuclei_pmaps, multicut

# path to the filtered image of GFP channel
FILTERED_IMAGE_PATH = "filename_GFP_filtered_clean.tif"

# path to the unfiltered raw image
RAW_IMAGE_PATH = "filename_GFP_clean.tif"

# path to the DAPI image
NUCLEI_IMAGE_PATH = "filename_DAPI_clean.tif"

# path to the tissue mask
TISSUE_MASK_PATH = "filename1_GFP_tissue_mask.tif"

# path the segmentation will be saved to
OUTPUT_PATH = "filename_segmentation.tif"

# size of the voxels in microns (note it is ZYX)
IMAGE_SCALE = np.array([0.39, 0.21, 0.21]) # ZYX in microns


"""
should not need to modify below this line.
"""

# boundary segmentation config
BOUNDARY_MODEL = "lightsheet_3D_unet_root_ds2x"
BOUNDARY_MODEL_SCALE = np.array([0.25, 0.325, 0.325])
BOUNDARY_PATCH_SIZE = (80, 160, 160)
BOUNDARY_STRIDE = STRIDE_ACCURATE


# nuclei segmentation config
NUCLEI_MODEL = "lightsheet_3D_unet_root_nuclei_ds1x"
NUCLEI_PATCH_SIZE = (80, 160, 160)
NUCLEI_STRIDE = STRIDE_ACCURATE


# load the images
filtered_image = imread(FILTERED_IMAGE_PATH)
raw_image = imread(RAW_IMAGE_PATH)
nuclei_image = imread(NUCLEI_IMAGE_PATH)
tissue_mask = imread(TISSUE_MASK_PATH).astype(bool)
print("images loaded")

# rescale the images
rescaling_factor = IMAGE_SCALE / BOUNDARY_MODEL_SCALE
print(rescaling_factor)
filtered_image_rescaled = image_rescale(
    image=filtered_image,
    factor=rescaling_factor,
    order=1
)
nuclei_image_rescaled = image_rescale(
    image=nuclei_image,
    factor=rescaling_factor,
    order=1
)
print("images rescaled")

# segment the boundaries
boundary_prediction = unet_predictions(
    raw=filtered_image_rescaled,
    model_name=BOUNDARY_MODEL,
    patch=BOUNDARY_PATCH_SIZE,
    stride=BOUNDARY_STRIDE,
    device="cuda",
    model_update=False,
    mirror_padding=(16, 32, 32)
)
print("boundaries predicted")

# segment the nuclei
nuclei_prediction = unet_predictions(
    raw=nuclei_image_rescaled,
    model_name=NUCLEI_MODEL,
    patch=NUCLEI_PATCH_SIZE,
    stride=NUCLEI_STRIDE,
    device="cuda",
    model_update=False,
    mirror_padding=(16, 32, 32)
)
print("nuclei predicted")

# Perform the watershed
watershed_segmentation = dt_watershed(
    boundary_pmaps=boundary_prediction,
    threshold=0.5,
    sigma_seeds=0.2,
    stacked=False,
    sigma_weights=2,
    min_size=100,
    alpha=1,
    pixel_pitch=(1, 1, 1),
    apply_nonmax_suppression=False,
    n_threads=None,
    mask=None
)
print("watershed done")

# agglomerate the watershed with lifted multicut
# raw_segmentation = lifted_multicut_from_nuclei_pmaps(
#     boundary_pmaps=boundary_prediction,
#     nuclei_pmaps=nuclei_prediction,
#     superpixels=watershed_segmentation,
#     beta=0.5,
#     post_minsize=100
# )
raw_segmentation = multicut(
    boundary_pmaps=boundary_prediction,
    superpixels=watershed_segmentation,
    beta=0.5,
    post_minsize=100
)

# put the segmentation back in the original scale
unrescale_factor = np.array(raw_image.shape) / np.array(raw_segmentation.shape)
original_scale_segmentation = image_rescale(
    image=raw_segmentation,
    factor=unrescale_factor,
    order=0
)
print(unrescale_factor)

# mask the image
original_scale_segmentation[tissue_mask] = 0

# relabel
clean_segmentation, _, _ = relabel_sequential(original_scale_segmentation)

print("segmentation done")
imsave(OUTPUT_PATH, clean_segmentation, check_contrast=False)