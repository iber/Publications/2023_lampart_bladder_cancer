# 2023_lampart_bladder_cancer

F. L. Lampart, R. Vetter, K. A. Yamauchi, Y. Wang, S. Runser, N. Strohmeyer, F. Meer, M-D. Hussherr, 
G. Camenisch, Hans-Helge Seifert, C. A. Rentsch, C. Le Magnen, D. J. Müller, L. Bubendorf, D. Iber

*Morphometry and mechanical instability at the onset of epithelial bladder cancer*

bioRxiv (2023)

https://doi.org/10.1101/2023.08.17.553533


## Contents

* Thickness Measurement
  * environment.yml
  * Mesh_generation.ipynb - Jupyter Notebook to generate meshes from Ilastik predictions
  * Dist_Measurement.ipynb - Jupyter Notebook to measure the distance between two surfaces
  * EP_mesh.obj - Epithelium test mesh
  * BM_mesh.obj - Basement membrane test mesh
* Segment Cell Boundaries
  * GFP_Boundary_filtering.ijm - Fiji macro for image preprocessing
  * preprocess_filtering.ipynb - Preprocessing of cell boundary images
  * segment_images - Segmentation of cell boundaries
  * currate_segmentation.py - Curation of cell segmentations
* Cell Shape Analysis
  * cell_shape_analyis_script.py - Morphometric measurements on segmented cells
  * requirements.txt - Python package dependencies
* Abaqus Simulation Files
  * job_file_abq.inp - Abaqus input file
  * Solid_shell_model_mesh.cae - Corresponding mesh file
* Spectral Analysis
  * spectralanalysis.m - Matlab script that computes spectral properties of images
  * run_analysis.m - Main Matlab script (full analysis)
  * images.zip - Zip archive of analysed SPIM and simulation height maps
* live_imaging_fixure
  * base.stl - 3D printer geometry file for the fixure base used in live imaging
  * mold.stl - 3D printer geometry file for the fixure mold used in live imaging
  
* LICENSE - License file


## Installation Thickness Measurements

You can install thickness-analysis via our conda environment file. To do so, first install anaconda or miniconda on
your computer. Then, download the environment.yml file (right click the link and "Save as..."). In your terminal,
navigate to the directory you downloaded the environment.yml file to:

```bash
cd <path/to/downloaded/environment.yml>
```

Then create the thickness-analysis environment using

```bash
conda env create -f environment.yml
```

Once the environment has been created, you can activate it and use thickness-analysis:

```bash
conda activate thickness-analysis
```

## Installation Segment Cell Boundaries

You can install 3D segmentation via our conda environment file. To do so, first install anaconda or miniconda on
your computer. Then, download the environment.yml file (right click the link and "Save as..."). In your terminal,
navigate to the directory you downloaded the environment.yml file to:

```bash
cd <path/to/downloaded/environment.yml>
```

Then create the thickness-analysis environment using

```bash
conda env create -f environment.yml
```

Once the environment has been created, you can activate it and use 3d-seg:

```bash
conda activate 3d-seg
```


## Installation Cell Shape Analysis

Create a virtual environment

```bash
python3 -m venv bladder_venv
```

Activate the virtual environment

```bash
source bladder_venv/bin/activate
```

Install the requirements

```bash
pip3 install -r requirements.txt
```

## License

All source code is released under the 3-Clause BSD license (see LICENSE for details).
