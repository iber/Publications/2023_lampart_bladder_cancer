#Copyright © 2023
#ETH Zurich
#Department of Biosystems Science and Engineering
#Franziska L. Lampart, Roman Vetter, Kevin A. Yamauchi, Yifan Wang, Steve Runser, Nico Strohmeyer, Florian Meer, Marie-Didiée Hussherr,
#Gieri Camenisch, Hans-Helge Seifert, Cyrill A. Rentsch, Clémentine Le Magnen, Daniel J. Müller, Lukas Bubendorf, Dagmar Iber

import numpy as np
import pandas as pd
from os import path, _exit, getcwd, mkdir
import trimesh as tm
import vtk
from tifffile import imread, imwrite
from tqdm import tqdm
from scipy import ndimage
from napari_process_points_and_surfaces import label_to_surface
from skimage.measure import regionprops
import csv



"""
This script takes as input the segmented image of a sample and compute the morphological features of each cell.
"""






#---------------------------------------------------------------------------------------------------------------
def remove_unconnected_regions(im:np.ndarray, background_labels = [0, 1, 2]):
    """
    Some labels have some times different regions in space that are unconneceted. This script
    detects these kinds of labels and only keep the largest region.

    Parameters:
    -----------
    im: np.ndarray
        A 3D numpy array with labels.

    background_labels: list
        A list of the labels that correspond to the background. These labels will be ignored.

    Returns:
    --------
    im: np.ndarray
        A 3D numpy array with labels. The unconnected regions have been removed.
    """

    #Get all the labels
    unique_label_lst = np.unique(im)


    #Create a copy of the image, all the filtering will be done on this copy
    im_copy = im.copy()

    #Loop over all the labels
    for label in tqdm(unique_label_lst, desc='Removing unconnected regions'):

        #Skip the background labels
        if label in background_labels: continue

        #Create a binary mask of the label
        binary_mask = (im_copy == label).astype(np.uint8)

        #Use ndimage.label to segment the binary mask. If the labels has unconnected regions,
        #then this regions will be marked with different segmentation labels
        labeled_mask, num_features = ndimage.label(binary_mask)

        #If the label has more than one region, then remove all the regions except the largest one
        if num_features > 1:

            #Get the size of each region
            region_sizes = ndimage.sum(binary_mask, labeled_mask, range(num_features + 1))

            #Find the largest region
            largest_region_label = np.argmax(region_sizes[1:]) + 1

            #Create a mask of the largest region
            filtered_region = labeled_mask == largest_region_label

            #Remove all the other regions from the image
            im_copy[binary_mask != filtered_region] = 0

    return im_copy
#---------------------------------------------------------------------------------------------------------------






#---------------------------------------------------------------------------------------------------------------
def smooth_labels(im:np.ndarray, erosion_iteration=1, dilation_iteration=2, background_labels = [0, 1, 2]):
    """
    Smooth the labels of the segmented images. This is done to remove some of the artefact created
    by the segmentation.

    Parameters:
    -----------

    im: np.ndarray
        A 3D numpy array with labels.

    erosion_iteration: int
        The number of erosion iterations to perform. A higher number will result in more smoothing.

    dilation_iteration: int
        The number of dilation iterations to perform. A higher number will result in more smoothing.

    background_labels: list
        A list of the labels that correspond to the background. These labels will be ignored.


    Returns:
    --------
    im: np.ndarray
        A 3D numpy array with labels. The labels have been smoothed.
    """


    #Create a copy of the image, all the filtering will be done on this copy
    smoothed_im = im.copy()


    #Loop over all the labels
    for label in tqdm(np.unique(smoothed_im), desc='Smoothing labels'):

        #We skip the background labels
        if label in background_labels: continue

        # Get the binary mask for the current label
        binary_mask = (smoothed_im == label).astype(np.uint8)

        # Erode the binary mask first
        eroded_mask = ndimage.binary_erosion(binary_mask, iterations=erosion_iteration)

        # Dilate the eroded mask
        dilated_mask = ndimage.binary_dilation(eroded_mask, iterations=dilation_iteration)


        #To prevent the dilated cell label to expand on the labels of its neighbors, we create a mask
        #of the background and only allow the dilated label to expand on the background
        background_mask = np.isin(smoothed_im, [0,1,2])

        #Combine the dilated mask and the background mask
        label_mask = np.logical_and(dilated_mask, background_mask)

        smoothed_im[dilated_mask] = label

    return smoothed_im
#---------------------------------------------------------------------------------------------------------------



#---------------------------------------------------------------------------------------------------------------
def generate_cell_meshes(
        im:np.ndarray,
        labels_to_convert,
        voxel_resolution: list,
        smoothing_iteration:int=10,
        pad_width:int=10,
    ):
    """
    Transform the segmented image into a list of meshes. Each mesh corresponds to a cell.

    Parameters:
    -----------

    im: np.ndarray
        A 3D numpy array with labels.

    labels_to_convert: list
        A list of the labels that will be converted into meshes.

    voxel_resolution: list
        The voxel resolution of the image. The list should have 3 elements, one for each axis.
        In this order [z_res, x_res, y_res]

    smoothing_iteration: int
        A laplacian smoothing is applied to the mesh, this parameter sets the number of iterations.

    pad_width: int
        The number of pixels to add to the border of the image. This is done to ensure that the
        cells in direct contact with the border are properly meshed.


    Returns:
    --------
    mesh_lst: list
        A list of meshes. Each mesh is a cell.
    """

    #Add some padding to the border of the image. Otherwise the cells in direct contact with the border
    #will not be properly meshed
    im_padded = np.pad(im, pad_width, mode='constant', constant_values=0)

    #Store the meshes in this list
    mesh_lst = []

    #Loop over all the labels
    for label in tqdm(labels_to_convert, desc='Generating meshes'):

        #Create a binary mask of the label
        label_mask = (im_padded == label).astype(np.uint8)

        #Use napari label_to_surface method to convert the labels into a mesh
        surface = label_to_surface(label_mask)
        points, faces = surface[0], surface[1]

        #Removing the padding and rescale the points to the voxel resolution
        points = (points - np.array([pad_width, pad_width, pad_width])) * voxel_resolution

        #Create a trimesh object
        mesh = tm.Trimesh(vertices=points, faces=faces)

        #Smooth the mesh of the cell. If this is not done, the mesh will have some creases/ridges at positions between the
        #z-slices of the segmented image. This creases/ridges will artificially increase the surface area of the cell.
        mesh = tm.smoothing.filter_mut_dif_laplacian(mesh, iterations=smoothing_iteration, lamb=0.5)

        #Add the mesh to the list
        mesh_lst.append(mesh)

    return mesh_lst
#---------------------------------------------------------------------------------------------------------------




#---------------------------------------------------------------------------------------------------------------
def save_meshes(
        mesh_label_lst:list,
        mesh_lst:list,
        output_folder_path:str
    ):
    """
    Save the mesh of each cell into an STL file

    Parameters:
    -----------
    mesh_label_lst: list
        The labels of the meshes

    im: np.ndarray
        A 3D numpy array with labels.

    mesh_lst: list
        A list of meshes. Each mesh is a cell.

    output_folder_path: str
        Path to the output folder.

    Returns:
    --------

    None
    """

    assert len(mesh_label_lst) == len(mesh_lst)

    #Create a folder to store the meshes if not already created
    if not path.exists(path.join(output_folder_path, "meshes")): mkdir(path.join(output_folder_path, "meshes"))

    #Save the meshes
    for cell_label, cell_mesh in tqdm(zip(mesh_label_lst, mesh_lst), desc='Saving meshes', total=len(mesh_label_lst)):
        cell_mesh.export(path.join(output_folder_path, "meshes", f"mesh_{cell_label}.stl"))

#---------------------------------------------------------------------------------------------------------------





#---------------------------------------------------------------------------------------------------------------
def compute_cell_areas(cell_mesh_lst:list):
    """
    Compute the surface area of each cell.

    Parameters:
    -----------

    cell_mesh_lst: list
        A list of meshes. Each mesh is a cell.

    Returns:
    --------

    cell_areas: list
        A list of the surface area of each cell.
    """

    #Store the surface area of each cell
    cell_areas = []

    #Compute the surface area of each cell
    for cell_mesh in tqdm(cell_mesh_lst, desc='Computing cell areas'):
        cell_areas.append(cell_mesh.area)

    return cell_areas
#---------------------------------------------------------------------------------------------------------------



#---------------------------------------------------------------------------------------------------------------
def compute_cell_volumes(cell_mesh_lst:list):
    """
    Compute the volume of each cell.

    Parameters:
    -----------

    cell_mesh_lst: list
        A list of meshes. Each mesh is a cell.

    Returns:
    --------

    cell_areas: list
        A list of the volume of each cell.
    """

    #Store the surface volume of each cell
    cell_volumes = []

    #Compute the surface area of each cell
    for cell_mesh in tqdm(cell_mesh_lst, desc='Computing cell volumes'):
        cell_volumes.append(cell_mesh.volume)

    return cell_volumes
#---------------------------------------------------------------------------------------------------------------




#---------------------------------------------------------------------------------------------------------------
def compute_cell_isoperimetric_ratios(cell_area_lst, cell_volume_lst):
    """
    Compute the isoperimetric ratio of each cell.

    Parameters:
    -----------
    cell_area_lst: list
        A list of the surface area of each cell.

    cell_volume_lst: list
        A list of the volume of each cell.

    Returns:
    --------

    cell_isoperimetric_ratios: list
        A list of the isoperimetric ratio of each cell.
    """

    isoperimetric_ratio_ar = np.power(np.array(cell_area_lst), 3) / np.power(np.array(cell_volume_lst), 2)
    return isoperimetric_ratio_ar.tolist()
#---------------------------------------------------------------------------------------------------------------




#---------------------------------------------------------------------------------------------------------------
def get_cell_adjacency_information(im: np.array, background_labels = [0, 1, 2]):
    """
    Get all the neighbors of a given cell. Two cells are considered neighborhs if
    a subset of their surfaces are directly touching.

    Parameters:
    -----------

    im: (np.array, 3D)
        The tirangular meshes of the cell in the standard trimesh format

    background_labels: list
        A list of the labels that correspond to the background. These labels will be ignored.

    Returns
    -------
    neighbors_lst: (list of str)
        For each cell return a string with the ids of the neighbors in the following format:
        "neighbor_id1 neighbor_id2 neighbor_id3 ..."


    nb_neighbors_lst: (list of int)
        Return for each cell the number of neighbors it has.
    """

    #Store the neighbors of each cell in this list
    neighbors_lst = []
    nb_neighbors_lst = []

    #Loop over the cell labels
    for cell_id in tqdm(np.unique(im), desc='Computing cell adjacency information'):

        #We skip the backround labels
        if cell_id in background_labels: continue

        #Create a mask of the cell
        mask = im == cell_id

        #Expand the volume of the cell by 2 voxels in each direction
        expanded_cell_voxels = ndimage.binary_dilation(mask, iterations=2)

        #Find the voxels that are directly in contact with the surface of the cell
        cell_surface_voxels = expanded_cell_voxels ^ mask

        #Get the labels of the neighbors
        cell_neighbors_lst = np.unique(im[cell_surface_voxels])

        #Remove the label of the cell itself from the list of neighbors
        cell_neighbors_lst = cell_neighbors_lst[(cell_neighbors_lst != cell_id)]

        #Convert the list of neighbors from a list to a string
        cell_neighbors_str = " ".join([str(neighbor) for neighbor in cell_neighbors_lst])
        neighbors_lst.append(cell_neighbors_str)

        #Remove the background labels when computing the number of neighbors
        cell_nb_neighbors = len([neighbor for neighbor in cell_neighbors_lst if neighbor not in [0,1,2]])
        nb_neighbors_lst.append(cell_nb_neighbors)

    return nb_neighbors_lst, neighbors_lst
#------------------------------------------------------------------------------------------------------------



#------------------------------------------------------------------------------------------------------------
def compute_cell_sphericity(cell_area_lst, cell_volume_lst):
    """
    Calculate sphericity based on area and volume.

    Parameters:
    -----------
    cell_area_lst: list
    A list of the surface area of each cell.

    cell_volume_lst: list
    A list of the volume of each cell.

    Returns:
    --------

    cell_isoperimetric_ratios: list
    A list of the spericity of each cell.

    """
    cell_sphericity = ((np.pi**(1/3))*((6*np.array(cell_volume_lst))**(2/3)))/np.array(cell_area_lst)
    return cell_sphericity


#------------------------------------------------------------------------------------------------------------


#------------------------------------------------------------------------------------------------------------
def compute_cell_elongation(img):
    """
    Calculate cell elongation

    Parameters:
    -----------
        img: imported tiff file

    Returns:
    --------

    cell_elongation: list
    A list of the elongation of each cell.

    """
    props = regionprops(img)
    elongation_data = []

    for prop in props:
        index = prop.label
        major_axis_length = prop.major_axis_length
        minor_axis_length = prop.minor_axis_length

        if index != 1 and index != 2:
            elongation = major_axis_length/minor_axis_length
            elongation_data.append(elongation)
    return elongation_data

#------------------------------------------------------------------------------------------------------------


#------------------------------------------------------------------------------------------------------------
def collect_cell_morphological_statistics(
        img_folder:str,
        img_name:str
    ):

    """
    Collect the morphological features of each cell and store the results in a csv file.

    Parameters:
    -----------

    img_folder: str
        Path to the folder containing the segmented image.

    img_name: str
        Name of the segmented image.

    Returns:
    --------

    None
    """


    #Absolute path to the segmented image
    img_path = path.join(img_folder, img_name)

    #Get the name of the image without the extension
    img_name = path.splitext(img_name)[0]

    #Read the image as a numpy array
    img = imread(img_path)

    #Remove the unconnected regions
    img = remove_unconnected_regions(img)

    #Smooth the labels of the image to remove segmentation artefacts
    img = smooth_labels(img)

    #Save the smoothed image
    imwrite(path.join(img_folder, img_name+"_smoothed.tiff"), img)

    #Get the mesh of each cell
    mesh_lst = generate_cell_meshes(img, labels_to_convert=np.unique(img)[3:], voxel_resolution=[0.5e-6, 0.325e-6, 0.325e-6])

    #Save the meshes of the cells
    save_meshes(np.unique(img)[3:], mesh_lst, img_folder)

    #Collect the morphological features of each cell
    cell_area_lst =     compute_cell_areas(mesh_lst)
    cell_volume_lst =   compute_cell_volumes(mesh_lst)
    cell_isoperimetric_ratio_lst = compute_cell_isoperimetric_ratios(cell_area_lst, cell_volume_lst)
    cell_nb_neighbors_lst, cell_neighbors_lst = get_cell_adjacency_information(img)
    cell_sphericity_lst = compute_cell_sphericity(cell_area_lst,cell_volume_lst)
    #cell_elongation_lst, cell_principal_axis_lst = compute_cell_principal_axis(mesh_lst)
    cell_elongation_lst = compute_cell_elongation(img)

    #Save the morphological features of each cell in a pandas dataframe
    df = pd.DataFrame({
        "cell_id": np.unique(img)[3:], #The 3 first labels are the background labels
        "cell_area": cell_area_lst,
        "cell_volume": cell_volume_lst,
        "cell_isoperimetric_ratio": cell_isoperimetric_ratio_lst,
        "cell_sphericity": cell_sphericity_lst,
        "cell_elongation": cell_elongation_lst,
        "cell_nb_neighbors": cell_nb_neighbors_lst,
        "cell_neighbors": cell_neighbors_lst,
    })

    #Save the dataframe as a csv file
    df.to_csv(path.join(img_folder,  img_name+"_morphological_features.csv"), index=False)

#------------------------------------------------------------------------------------------------------------







#---------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":

    #Path to the output folder
    img_folder_path = path.join(getcwd(), "images")

    #The name of the segmented image
    img_name = "esophagus.tif"

    #Collect the morphological features of each cell
    collect_cell_morphological_statistics(img_folder_path, img_name)
